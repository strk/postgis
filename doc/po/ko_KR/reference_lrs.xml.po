# SOME DESCRIPTIVE TITLE.
#
# Translators:
# Kwon.Yongchan <ruvyn@naver.com>, 2016
# Kwon.Yongchan <ruvyn@naver.com>, 2016
msgid ""
msgstr ""
"Project-Id-Version: PostGIS\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-29 10:57+0000\n"
"PO-Revision-Date: 2018-07-23 18:01+0000\n"
"Last-Translator: Regina Obe\n"
"Language-Team: Korean (Korea) (http://www.transifex.com/postgis/postgis/"
"language/ko_KR/)\n"
"Language: ko_KR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. Tag: title
#: reference_lrs.xml:3
#, no-c-format
msgid "Linear Referencing"
msgstr "선형 참조(Linear Referencing)"

#. Tag: refpurpose
#: reference_lrs.xml:9
#, no-c-format
msgid "Returns a point interpolated along a line at a fractional location."
msgstr ""

#. Tag: title
#: reference_lrs.xml:31 reference_lrs.xml:111 reference_lrs.xml:180
#: reference_lrs.xml:252 reference_lrs.xml:309 reference_lrs.xml:398
#: reference_lrs.xml:454 reference_lrs.xml:521 reference_lrs.xml:567
#: reference_lrs.xml:614
#, no-c-format
msgid "Description"
msgstr "설명"

#. Tag: para
#: reference_lrs.xml:33
#, fuzzy, no-c-format
msgid ""
"Returns a point interpolated along a line at a fractional location. First "
"argument must be a LINESTRING. Second argument is a float between 0 and 1 "
"representing the fraction of line length where the point is to be located. "
"The Z and M values are interpolated if present."
msgstr ""
"라인을 따라 보간된 포인트를 반환합니다. 첫 번째 인수는 라인스트링이어야 합니"
"다. 두 번째 인수는 0과 1 사이의 Float8 데이터형으로 라인스트링의 전체 길이에"
"서 포인트가 위치해야 하는 비율을 의미합니다."

#. Tag: para
#: reference_lrs.xml:41
#, no-c-format
msgid ""
"See <xref linkend=\"ST_LineLocatePoint\"/> for computing the line location "
"nearest to a Point."
msgstr ""
"포인트에 가장 가까운 라인의 위치를 계산하는 방법에 대해서는 <xref "
"linkend=\"ST_LineLocatePoint\"/> 를 참조하십시오."

#. Tag: para
#: reference_lrs.xml:45
#, no-c-format
msgid ""
"This function computes points in 2D and then interpolates values for Z and "
"M, while <xref linkend=\"ST_3DLineInterpolatePoint\"/> computes points in 3D "
"and only interpolates the M value."
msgstr ""

#. Tag: para
#: reference_lrs.xml:52
#, no-c-format
msgid ""
"Since release 1.1.1 this function also interpolates M and Z values (when "
"present), while prior releases set them to 0.0."
msgstr ""
"1.1.1 배포판부터 이 함수는 M 및 Z값(이 있을 경우)도 보간합니다. 이전 배포판에"
"서는 두 값을 0.0으로 고정시켰습니다."

#. Tag: para
#: reference_lrs.xml:56
#, no-c-format
msgid "Availability: 0.8.2, Z and M supported added in 1.1.1"
msgstr ""
"0.8.2 버전부터 사용할 수 있습니다. 1.1.1 버전에서 Z과 M 좌표를 지원합니다."

#. Tag: para
#: reference_lrs.xml:57
#, no-c-format
msgid "Changed: 2.1.0. Up to 2.0.x this was called ST_Line_Interpolate_Point."
msgstr ""
"변경 사항: 2.1.0 미만 버전, 즉 2.0.x 버전까지 이 함수의 명칭은 "
"ST_Line_Interpolate_Point였습니다."

#. Tag: para
#: reference_lrs.xml:58 reference_lrs.xml:127 reference_lrs.xml:197
#: reference_lrs.xml:339 reference_lrs.xml:531 reference_lrs.xml:576
#: reference_lrs.xml:620
#, no-c-format
msgid "&Z_support;"
msgstr "&Z_support;"

#. Tag: title
#: reference_lrs.xml:63 reference_lrs.xml:132 reference_lrs.xml:202
#: reference_lrs.xml:269 reference_lrs.xml:343 reference_lrs.xml:422
#: reference_lrs.xml:476 reference_lrs.xml:535 reference_lrs.xml:580
#: reference_lrs.xml:624
#, no-c-format
msgid "Examples"
msgstr "예시"

#. Tag: para
#: reference_lrs.xml:69
#, fuzzy, no-c-format
msgid "A LineString with the interpolated point at 20% position (0.20)"
msgstr "20% 위치 (0.20)에 있는 보간된 포인트를 가진 라인스트링"

#. Tag: para
#: reference_lrs.xml:74
#, no-c-format
msgid "The mid-point of a 3D line:"
msgstr ""

#. Tag: para
#: reference_lrs.xml:77
#, no-c-format
msgid "The closest point on a line to a point:"
msgstr ""

#. Tag: title
#: reference_lrs.xml:83 reference_lrs.xml:140 reference_lrs.xml:215
#: reference_lrs.xml:276 reference_lrs.xml:373 reference_lrs.xml:427
#: reference_lrs.xml:494 reference_lrs.xml:541 reference_lrs.xml:586
#, no-c-format
msgid "See Also"
msgstr "참고"

#. Tag: para
#: reference_lrs.xml:85
#, fuzzy, no-c-format
msgid ""
", <xref linkend=\"ST_3DLineInterpolatePoint\"/>, <xref "
"linkend=\"ST_LineLocatePoint\"/>"
msgstr ""
", <xref linkend=\"ST_LineInterpolatePoint\"/>, <xref "
"linkend=\"ST_LineMerge\"/>"

#. Tag: refpurpose
#: reference_lrs.xml:97
#, no-c-format
msgid "Returns a point interpolated along a 3D line at a fractional location."
msgstr ""

#. Tag: funcprototype
#: reference_lrs.xml:102
#, fuzzy, no-c-format
msgid ""
"<funcdef>geometry <function>ST_3DLineInterpolatePoint</function></funcdef> "
"<paramdef><type>geometry </type> <parameter>a_linestring</parameter></"
"paramdef> <paramdef><type>float8 </type> <parameter>a_fraction</parameter></"
"paramdef>"
msgstr ""
"<funcdef>geometry <function>ST_LineInterpolatePoint</function></funcdef> "
"<paramdef><type>geometry </type> <parameter>a_linestring</parameter></"
"paramdef> <paramdef><type>float8 </type> <parameter>a_fraction</parameter></"
"paramdef>"

#. Tag: para
#: reference_lrs.xml:113
#, fuzzy, no-c-format
msgid ""
"Returns a point interpolated along a 3D line at a fractional location. First "
"argument must be a LINESTRING. Second argument is a float between 0 and 1 "
"representing the point location as a fraction of line length. The M value is "
"interpolated if present."
msgstr ""
"라인을 따라 보간된 포인트를 반환합니다. 첫 번째 인수는 라인스트링이어야 합니"
"다. 두 번째 인수는 0과 1 사이의 Float8 데이터형으로 라인스트링의 전체 길이에"
"서 포인트가 위치해야 하는 비율을 의미합니다."

#. Tag: para
#: reference_lrs.xml:120
#, no-c-format
msgid ""
"computes points in 2D and then interpolates the values for Z and M, while "
"this function computes points in 3D and only interpolates the M value."
msgstr ""

#. Tag: para
#: reference_lrs.xml:126
#, fuzzy, no-c-format
msgid "Availability: 3.0.0"
msgstr "2.0.0 버전부터 사용할 수 있습니다."

#. Tag: para
#: reference_lrs.xml:134
#, no-c-format
msgid "Return point 20% along 3D line"
msgstr ""

#. Tag: para
#: reference_lrs.xml:142
#, fuzzy, no-c-format
msgid ""
", <xref linkend=\"ST_LineInterpolatePoints\"/>, <xref "
"linkend=\"ST_LineLocatePoint\"/>"
msgstr ""
", <xref linkend=\"ST_LineInterpolatePoint\"/>, <xref "
"linkend=\"ST_LineMerge\"/>"

#. Tag: refpurpose
#: reference_lrs.xml:154
#, no-c-format
msgid "Returns points interpolated along a line at a fractional interval."
msgstr ""

#. Tag: para
#: reference_lrs.xml:182
#, no-c-format
msgid ""
"Returns one or more points interpolated along a line at a fractional "
"interval. The first argument must be a LINESTRING. The second argument is a "
"float8 between 0 and 1 representing the spacing between the points as a "
"fraction of line length. If the third argument is false, at most one point "
"will be constructed (which is equivalent to <xref "
"linkend=\"ST_LineInterpolatePoint\"/>.)"
msgstr ""

#. Tag: para
#: reference_lrs.xml:190
#, no-c-format
msgid ""
"If the result has zero or one points, it is returned as a POINT. If it has "
"two or more points, it is returned as a MULTIPOINT."
msgstr ""

#. Tag: para
#: reference_lrs.xml:196
#, no-c-format
msgid "Availability: 2.5.0"
msgstr ""

#. Tag: para
#: reference_lrs.xml:198 reference_lrs.xml:416 reference_lrs.xml:471
#, no-c-format
msgid "&M_support;"
msgstr "&M_support;"

#. Tag: para
#: reference_lrs.xml:208
#, no-c-format
msgid "A LineString with points interpolated every 20%"
msgstr ""

#. Tag: para
#: reference_lrs.xml:217
#, no-c-format
msgid ", <xref linkend=\"ST_LineLocatePoint\"/>"
msgstr ""

#. Tag: refpurpose
#: reference_lrs.xml:228
#, no-c-format
msgid ""
"Returns the fractional location of the closest point on a line to a point."
msgstr ""

#. Tag: para
#: reference_lrs.xml:254
#, fuzzy, no-c-format
msgid ""
"Returns a float between 0 and 1 representing the location of the closest "
"point on a LineString to the given Point, as a fraction of <link "
"linkend=\"ST_Length2D\">2d line</link> length."
msgstr ""
"입력 포인트에 가장 가까운 위치에 있는 라인스트링 상의 포인트를 나타내는 "
"<link linkend=\"ST_Length2D\">2차원 라인</link> 전체 길이의 비율을 0에서 1 사"
"이의 부동소수점 데이터형(float)으로 반환합니다."

#. Tag: para
#: reference_lrs.xml:258
#, no-c-format
msgid ""
"You can use the returned location to extract a Point (<xref "
"linkend=\"ST_LineInterpolatePoint\"/>) or a substring (<xref "
"linkend=\"ST_LineSubstring\"/>)."
msgstr ""
"반환된 위치를 이용해서 포인트(<xref linkend=\"ST_LineInterpolatePoint\"/>) 또"
"는 부분 스트링(<xref linkend=\"ST_LineSubstring\"/>)을 추출할 수 있습니다."

#. Tag: para
#: reference_lrs.xml:261
#, no-c-format
msgid "This is useful for approximating numbers of addresses"
msgstr "이 함수는 주소의 개수의 근사치를 구하는 데 유용합니다."

#. Tag: para
#: reference_lrs.xml:263
#, no-c-format
msgid "Availability: 1.1.0"
msgstr "1.1.0 버전부터 사용할 수 있습니다."

#. Tag: para
#: reference_lrs.xml:264
#, no-c-format
msgid "Changed: 2.1.0. Up to 2.0.x this was called ST_Line_Locate_Point."
msgstr ""
"변경 사항: 2.1.0 미만 버전, 즉 2.0.x 버전까지 이 함수의 명칭은 "
"ST_Line_Locate_Point였습니다."

#. Tag: para
#: reference_lrs.xml:278
#, no-c-format
msgid ""
", <xref linkend=\"ST_Length2D\"/>, <xref linkend=\"ST_LineInterpolatePoint\"/"
">, <xref linkend=\"ST_LineSubstring\"/>"
msgstr ""
", <xref linkend=\"ST_Length2D\"/>, <xref linkend=\"ST_LineInterpolatePoint\"/"
">, <xref linkend=\"ST_LineSubstring\"/>"

#. Tag: refpurpose
#: reference_lrs.xml:286
#, no-c-format
msgid "Returns the part of a line between two fractional locations."
msgstr ""

#. Tag: para
#: reference_lrs.xml:311
#, no-c-format
msgid ""
"Computes the line which is the section of the input line starting and ending "
"at the given fractional locations. The first argument must be a LINESTRING. "
"The second and third arguments are values in the range [0, 1] representing "
"the start and end locations as fractions of line length. The Z and M values "
"are interpolated for added endpoints if present."
msgstr ""

#. Tag: para
#: reference_lrs.xml:320
#, fuzzy, no-c-format
msgid ""
"If <varname>startfraction</varname> and <varname>endfraction</varname> have "
"the same value this is equivalent to <xref "
"linkend=\"ST_LineInterpolatePoint\"/>."
msgstr ""
"'시작'과 '끝'이 동일한 값일 경우 이 함수는 <xref "
"linkend=\"ST_LineInterpolatePoint\"/> 함수와 같아집니다."

#. Tag: para
#: reference_lrs.xml:325
#, no-c-format
msgid ""
"This only works with LINESTRINGs. To use on contiguous MULTILINESTRINGs "
"first join them with <xref linkend=\"ST_LineMerge\"/>."
msgstr ""

#. Tag: para
#: reference_lrs.xml:331
#, fuzzy, no-c-format
msgid ""
"Since release 1.1.1 this function interpolates M and Z values. Prior "
"releases set Z and M to unspecified values."
msgstr ""
"1.1.1 배포판부터 이 함수는 M 및 Z값(이 있을 경우)도 보간합니다. 이전 배포판에"
"서는 두 값을 설정하지 않았습니다."

#. Tag: para
#: reference_lrs.xml:336
#, no-c-format
msgid "Enhanced: 3.4.0 - Support for geography was introduced."
msgstr ""

#. Tag: para
#: reference_lrs.xml:337
#, no-c-format
msgid "Changed: 2.1.0. Up to 2.0.x this was called ST_Line_Substring."
msgstr ""
"변경 사항: 2.1.0 미만 버전, 즉 2.0.x 버전까지 이 함수의 명칭은 "
"ST_Line_Substring이었습니다."

#. Tag: para
#: reference_lrs.xml:338
#, no-c-format
msgid "Availability: 1.1.0, Z and M supported added in 1.1.1"
msgstr "1.1.0 버전부터 사용할 수 있습니다. 1.1.1 버전부터 Z 및 M을 지원합니다."

#. Tag: para
#: reference_lrs.xml:349
#, fuzzy, no-c-format
msgid "A LineString seen with 1/3 midrange overlaid (0.333, 0.666)"
msgstr "중간 1/3 범위(0.333, 0.666)를 중첩해서 출력한 라인스트링"

#. Tag: para
#: reference_lrs.xml:354
#, no-c-format
msgid "If start and end locations are the same, the result is a POINT."
msgstr ""

#. Tag: para
#: reference_lrs.xml:360
#, no-c-format
msgid ""
"A query to cut a LineString into sections of length 100 or shorter. It uses "
"<varname>generate_series()</varname> with a CROSS JOIN LATERAL to produce "
"the equivalent of a FOR loop."
msgstr ""

#. Tag: para
#: reference_lrs.xml:367
#, no-c-format
msgid ""
"Geography implementation measures along a spheroid, geometry along a line"
msgstr ""

#. Tag: para
#: reference_lrs.xml:375
#, no-c-format
msgid ""
", <xref linkend=\"ST_LineInterpolatePoint\"/>, <xref "
"linkend=\"ST_LineMerge\"/>"
msgstr ""
", <xref linkend=\"ST_LineInterpolatePoint\"/>, <xref "
"linkend=\"ST_LineMerge\"/>"

#. Tag: refpurpose
#: reference_lrs.xml:383
#, no-c-format
msgid "Returns the point(s) on a geometry that match a measure value."
msgstr ""

#. Tag: funcprototype
#: reference_lrs.xml:388
#, fuzzy, no-c-format
msgid ""
"<funcdef>geometry <function>ST_LocateAlong</function></funcdef> "
"<paramdef><type>geometry </type> <parameter>geom_with_measure</parameter></"
"paramdef> <paramdef><type>float8 </type> <parameter>measure</parameter></"
"paramdef> <paramdef choice=\"opt\"><type>float8 </type> <parameter>offset = "
"0</parameter></paramdef>"
msgstr ""
"<funcdef>geometry <function>ST_LocateAlong</function></funcdef> "
"<paramdef><type>geometry </type> <parameter>ageom_with_measure</parameter></"
"paramdef> <paramdef><type>float8 </type> <parameter>a_measure</parameter></"
"paramdef> <paramdef choice=\"opt\"><type>float8 </type> <parameter>offset</"
"parameter></paramdef>"

#. Tag: para
#: reference_lrs.xml:400
#, no-c-format
msgid ""
"Returns the location(s) along a measured geometry that have the given "
"measure values. The result is a Point or MultiPoint. Polygonal inputs are "
"not supported."
msgstr ""

#. Tag: para
#: reference_lrs.xml:405
#, no-c-format
msgid ""
"If <varname>offset</varname> is provided, the result is offset to the left "
"or right of the input line by the specified distance. A positive offset will "
"be to the left, and a negative one to the right."
msgstr ""

#. Tag: para
#: reference_lrs.xml:409 reference_lrs.xml:572
#, no-c-format
msgid "Use this function only for linear geometries with an M component"
msgstr ""

#. Tag: para
#: reference_lrs.xml:411 reference_lrs.xml:465
#, no-c-format
msgid ""
"The semantic is specified by the <emphasis>ISO/IEC 13249-3 SQL/MM Spatial</"
"emphasis> standard."
msgstr ""

#. Tag: para
#: reference_lrs.xml:413
#, no-c-format
msgid "Availability: 1.1.0 by old name ST_Locate_Along_Measure."
msgstr ""
"1.1.0 버전부터 예전 명칭인 ST_Locate_Along_Measure로 사용할 수 있습니다."

#. Tag: para
#: reference_lrs.xml:414
#, fuzzy, no-c-format
msgid ""
"Changed: 2.0.0 in prior versions this used to be called "
"ST_Locate_Along_Measure."
msgstr ""
"변경 사항: 2.0.0 미만 버전에서는 ST_Locate_Along_Measure라는 명칭이었습니다. "
"예전 명칭은 더 지원되지 않고 곧 삭제될 예정이지만, 아직은 사용할 수 있습니다."

#. Tag: para
#: reference_lrs.xml:417
#, no-c-format
msgid "&sqlmm_compliant; SQL-MM IEC 13249-3: 5.1.13"
msgstr ""

#. Tag: para
#: reference_lrs.xml:429
#, no-c-format
msgid ""
", <xref linkend=\"ST_LocateBetweenElevations\"/>, <xref "
"linkend=\"ST_InterpolatePoint\"/>"
msgstr ""

#. Tag: refpurpose
#: reference_lrs.xml:437
#, no-c-format
msgid "Returns the portions of a geometry that match a measure range."
msgstr ""

#. Tag: funcprototype
#: reference_lrs.xml:442
#, fuzzy, no-c-format
msgid ""
"<funcdef>geometry <function>ST_LocateBetween</function></funcdef> "
"<paramdef><type>geometry </type> <parameter>geom</parameter></paramdef> "
"<paramdef><type>float8 </type> <parameter>measure_start</parameter></"
"paramdef> <paramdef><type>float8 </type> <parameter>measure_end</parameter></"
"paramdef> <paramdef choice=\"opt\"><type>float8 </type> <parameter>offset = "
"0</parameter></paramdef>"
msgstr ""
"<funcdef>geometry <function>ST_LocateBetween</function></funcdef> "
"<paramdef><type>geometry </type> <parameter>geomA</parameter></paramdef> "
"<paramdef><type>float8 </type> <parameter>measure_start</parameter></"
"paramdef> <paramdef><type>float8 </type> <parameter>measure_end</parameter></"
"paramdef> <paramdef choice=\"opt\"><type>float8 </type> <parameter>offset</"
"parameter></paramdef>"

#. Tag: para
#: reference_lrs.xml:456
#, fuzzy, no-c-format
msgid ""
"Return a geometry (collection) with the portions of the input measured "
"geometry that match the specified measure range (inclusively)."
msgstr ""
"설정한 척도와 일치하는 요소들을 가진 파생 도형 집합의 값을 반환합니다. 폴리"
"곤 구성 요소는 지원되지 않습니다."

#. Tag: para
#: reference_lrs.xml:459
#, fuzzy, no-c-format
msgid ""
"If the <varname>offset</varname> is provided, the result is offset to the "
"left or right of the input line by the specified distance. A positive offset "
"will be to the left, and a negative one to the right."
msgstr ""
"오프셋을 설정할 경우, 그 결과 입력 라인에서 설정한 단위 개수만큼 왼쪽 또는 오"
"른쪽에 오프셋 도형 집합을 출력할 것입니다. 양의 오프셋은 왼쪽, 음의 오프셋은 "
"오른쪽으로 출력할 것입니다."

#. Tag: para
#: reference_lrs.xml:463 reference_lrs.xml:526
#, no-c-format
msgid "Clipping a non-convex POLYGON may produce invalid geometry."
msgstr ""

#. Tag: para
#: reference_lrs.xml:467
#, no-c-format
msgid "Availability: 1.1.0 by old name ST_Locate_Between_Measures."
msgstr ""
"1.1.0 버전부터 예전 명칭인 ST_Locate_Between_Measures로 사용할 수 있습니다."

#. Tag: para
#: reference_lrs.xml:468
#, fuzzy, no-c-format
msgid ""
"Changed: 2.0.0 - in prior versions this used to be called "
"ST_Locate_Between_Measures."
msgstr ""
"변경 사항: 2.0.0 미만 버전에서는 ST_Locate_Along_Measure라는 명칭이었습니다. "
"예전 명칭은 더 지원되지 않고 곧 삭제될 예정이지만, 아직은 사용할 수 있습니다."

#. Tag: para
#: reference_lrs.xml:469 reference_lrs.xml:529
#, no-c-format
msgid "Enhanced: 3.0.0 - added support for POLYGON, TIN, TRIANGLE."
msgstr ""

#. Tag: para
#: reference_lrs.xml:472
#, no-c-format
msgid "&sqlmm_compliant; SQL-MM IEC 13249-3: 5.1"
msgstr ""

#. Tag: para
#: reference_lrs.xml:485
#, no-c-format
msgid ""
"A LineString with the section between measures 2 and 8, offset to the left"
msgstr ""

#. Tag: para
#: reference_lrs.xml:496
#, fuzzy, no-c-format
msgid ", <xref linkend=\"ST_LocateBetweenElevations\"/>"
msgstr ""
", <xref linkend=\"ST_LocateAlong\"/>, <xref linkend=\"ST_LocateBetween\"/>"

#. Tag: refpurpose
#: reference_lrs.xml:504
#, no-c-format
msgid "Returns the portions of a geometry that lie in an elevation (Z) range."
msgstr ""

#. Tag: funcprototype
#: reference_lrs.xml:510
#, fuzzy, no-c-format
msgid ""
"<funcdef>geometry <function>ST_LocateBetweenElevations</function></funcdef> "
"<paramdef><type>geometry </type> <parameter>geom</parameter></paramdef> "
"<paramdef><type>float8 </type> <parameter>elevation_start</parameter></"
"paramdef> <paramdef><type>float8 </type> <parameter>elevation_end</"
"parameter></paramdef>"
msgstr ""
"<funcdef>geometry <function>ST_LocateBetweenElevations</function></funcdef> "
"<paramdef><type>geometry </type> <parameter>geom_mline</parameter></"
"paramdef> <paramdef><type>float8 </type> <parameter>elevation_start</"
"parameter></paramdef> <paramdef><type>float8 </type> "
"<parameter>elevation_end</parameter></paramdef>"

#. Tag: para
#: reference_lrs.xml:523
#, no-c-format
msgid ""
"Returns a geometry (collection) with the portions of a geometry that lie in "
"an elevation (Z) range."
msgstr ""

#. Tag: para
#: reference_lrs.xml:528
#, no-c-format
msgid "Availability: 1.4.0"
msgstr "1.4.0 버전부터 사용할 수 있습니다."

#. Tag: para
#: reference_lrs.xml:543
#, fuzzy, no-c-format
msgid ", <xref linkend=\"ST_LocateBetween\"/>"
msgstr ""
", <xref linkend=\"ST_LocateAlong\"/>, <xref linkend=\"ST_LocateBetween\"/>"

#. Tag: refpurpose
#: reference_lrs.xml:552
#, no-c-format
msgid "Returns the interpolated measure of a geometry closest to a point."
msgstr ""
"입력 포인트에 가까운 포인트에서 도형의 척도 차원(M 차원)의 값을 반환합니다."

#. Tag: funcprototype
#: reference_lrs.xml:557
#, no-c-format
msgid ""
"<funcdef>float8 <function>ST_InterpolatePoint</function></funcdef> "
"<paramdef><type>geometry </type> <parameter>linear_geom_with_measure</"
"parameter></paramdef> <paramdef><type>geometry </type> <parameter>point</"
"parameter></paramdef>"
msgstr ""

#. Tag: para
#: reference_lrs.xml:569
#, no-c-format
msgid ""
"Returns an interpolated measure value of a linear measured geometry at the "
"location closest to the given point."
msgstr ""

#. Tag: para
#: reference_lrs.xml:574
#, no-c-format
msgid "Availability: 2.0.0"
msgstr "2.0.0 버전부터 사용할 수 있습니다."

#. Tag: para
#: reference_lrs.xml:588
#, no-c-format
msgid ""
", <xref linkend=\"ST_LocateAlong\"/>, <xref linkend=\"ST_LocateBetween\"/>"
msgstr ""
", <xref linkend=\"ST_LocateAlong\"/>, <xref linkend=\"ST_LocateBetween\"/>"

#. Tag: refpurpose
#: reference_lrs.xml:598
#, no-c-format
msgid "Interpolates measures along a linear geometry."
msgstr ""

#. Tag: funcprototype
#: reference_lrs.xml:603
#, no-c-format
msgid ""
"<funcdef>geometry <function>ST_AddMeasure</function></funcdef> "
"<paramdef><type>geometry </type> <parameter>geom_mline</parameter></"
"paramdef> <paramdef><type>float8 </type> <parameter>measure_start</"
"parameter></paramdef> <paramdef><type>float8 </type> <parameter>measure_end</"
"parameter></paramdef>"
msgstr ""
"<funcdef>geometry <function>ST_AddMeasure</function></funcdef> "
"<paramdef><type>geometry </type> <parameter>geom_mline</parameter></"
"paramdef> <paramdef><type>float8 </type> <parameter>measure_start</"
"parameter></paramdef> <paramdef><type>float8 </type> <parameter>measure_end</"
"parameter></paramdef>"

#. Tag: para
#: reference_lrs.xml:616
#, fuzzy, no-c-format
msgid ""
"Return a derived geometry with measure values linearly interpolated between "
"the start and end points. If the geometry has no measure dimension, one is "
"added. If the geometry has a measure dimension, it is over-written with new "
"values. Only LINESTRINGS and MULTILINESTRINGS are supported."
msgstr ""
"시작점과 종단점 사이의 선형적으로 보간된 척도 요소들을 가진 파생 도형을 반환"
"합니다. 도형이 척도 차원을 가지고 있지 않을 경우, 척도 차원을 추가합니다. 도"
"형이 척도 차원을 가지고 있을 경우, 새 값들로 덮어 씁니다. 라인스트링과 멀티라"
"인스트링만 지원합니다."

#. Tag: para
#: reference_lrs.xml:618
#, no-c-format
msgid "Availability: 1.5.0"
msgstr "1.5.0 버전부터 사용할 수 있습니다."
